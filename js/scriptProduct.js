produkt = {}
const id = new URLSearchParams(window.location.search).get('id');

async function init_website() {
    //Get ID from url
    produkt = await GetProdukt(`../php/apiHandler.php?doGetCoffee&id=${id}`);
    await generateData()
    
}

async function generateData() {
    const content = document.querySelector(".content");
    const nadpis = document.querySelector(".nadpis");
    const popis = document.querySelector(".popis");
    const img = document.querySelector(".img");

    // Generate code
    nadpis.textContent = produkt[0].nazev;
    popis.textContent = produkt[0].popis;
    img.src = produkt[0].image;
}

init_website()